{-# LANGUAGE PackageImports #-}
import "reflect-haskell" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
